package com.mindvalley_aiyazparmar_android_test.utils;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;
import com.mindvalley_aiyazparmar_android_test.R;

/**
 * Created by Aiyaz Parmar on 11/19/2016.
 */

public class Utils {


    public static MaterialDialog getProgressDialog(Context context){
        MaterialDialog.Builder materialDialog=new MaterialDialog.Builder(context)
                .content(R.string.please_wait)
                .progressIndeterminateStyle(false)
                .progress(true,0)
                ;
        return materialDialog.build();
    }
}
