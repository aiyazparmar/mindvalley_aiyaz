package com.mindvalley_aiyazparmar_android_test.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;


/**
 * Created by Aiyaz Parmar on 11/19/2016.
 */

public interface OnCallWebServiceListener {
    void OnStartingService();
    void OnGettingResult(JSONObject jsonObject,int flag) throws JSONException, MethodNotDefinedException ;
    void OnGettingResult(JSONArray jsonObject, int flag) throws JSONException, MethodNotDefinedException ;
    void OnGettingResult(String xmlString, int flag) throws MethodNotDefinedException ;
}
