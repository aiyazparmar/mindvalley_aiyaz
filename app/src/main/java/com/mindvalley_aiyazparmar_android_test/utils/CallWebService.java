package com.mindvalley_aiyazparmar_android_test.utils;

import android.os.AsyncTask;
import android.util.Log;


import com.mindvalley_aiyazparmar_android_test.url_caching.LruCache;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Aiyaz Parmar on 11/19/16.
 */
public class CallWebService {


    public static final int JSON = 0;
    public static final int XML = 1;


    public static final int GET = 0;
    public static final int POST = 1;

    public int flag;

    OnCallWebServiceListener onCallWebServiceListener;
    private HashMap<String, String> apiParameters;
    private String WebService;
    static LruCache lruCache=new LruCache();



    /*Here responseType stands for in which Type of response you will get like xml or json
    method type will be get or post you have to pass which type of webservice you using
    url will be your webservice url
    apiParameters -- api parameters is for parameter when you using post and you want to pass params
                    then you can do that with HashMap apiParameter.

                    like you have to pass something like this
                            HashMap<String,String> apiParams=new HashMap<>();
                            apiParams.put("name","aiyaz");

                    now above HashMap object will pass parameter "name" with value "aiyaz" in post
                    url.

    OnCallWebServiceListener --  this interface is for getting states of webservice.like when you call
                            your webservice it will first call on OnStartingService(); and when you get your result
                            then it will call OnGettingResult(...); you have to implement this interface
                            in your class from where you are calling

    flag -- flag is to differentiate which webservice call returned to OnGettingResult(...);
            if you call two webservice concurrently than it will be helpful to differentiate that of which
            webservice result you got in OnGettingResult(...);


    */

    public CallWebService(int responseType, int methodType, String url, HashMap<String, String> apiParameters, OnCallWebServiceListener onCallWebServiceListener, int flag) throws MethodNotDefinedException {
        this.onCallWebServiceListener = onCallWebServiceListener;
        this.flag=flag;

        boolean isCacheDataFound=false;
        if(responseType==XML) {
            if (lruCache.getResponse(url+"-xml")!=null){
                    onCallWebServiceListener.OnGettingResult(lruCache.getResponse(url+"-xml"),flag);
                    isCacheDataFound=true;
            }
        }else{
            if (lruCache.getResponse(url+"-json")!=null){
                try {
                Object json = new JSONTokener(lruCache.getResponse(url+"-json")).nextValue();
                if(json instanceof JSONObject){
                    onCallWebServiceListener.OnGettingResult(new JSONObject(lruCache.getResponse(url+"-json")),flag);
                }
                else if(json instanceof JSONArray) {
                    onCallWebServiceListener.OnGettingResult(new JSONArray(lruCache.getResponse(url + "-json")), flag);
                }

                    isCacheDataFound=true;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        if(!isCacheDataFound) {
                if (responseType == JSON || responseType == XML) {
                    if (methodType == GET || methodType == POST) {
                        WebService = url;
                        if (WebService != null) {
                            if (apiParameters != null) {
                                this.apiParameters = apiParameters;
                                new ExecuteAsyncTask(methodType, responseType).execute(apiParameters);
                            } else {
                                new ExecuteAsyncTask(methodType, responseType).execute();
                            }
                        } else {
                            throw new NullPointerException("Please define webservice url.");
                        }
                    } else {
                        throw new MethodNotDefinedException("Define method for webservice.");
                    }
                } else {
                    throw new MethodNotDefinedException("Please define response type for webservice");
                }
        }else{

        }
    }

    private class ExecuteAsyncTask extends AsyncTask<HashMap<String, String>, Void, Object> {
        int MethodType;
        int responseType;

        public ExecuteAsyncTask(int MethodType, int responseType) {
            this.MethodType = MethodType;
            this.responseType = responseType;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            onCallWebServiceListener.OnStartingService();
        }

        @Override
        protected void onPostExecute(Object object) {
            super.onPostExecute(object);
            if (object instanceof JSONObject) {
                try {
                    onCallWebServiceListener.OnGettingResult((JSONObject) object, flag);
                    lruCache.putResponse(WebService+"-json",((JSONObject) object).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (MethodNotDefinedException e) {
                    e.printStackTrace();
                }
            }else if(object instanceof JSONArray){
                try {
                    onCallWebServiceListener.OnGettingResult((JSONArray) object, flag);
                    lruCache.putResponse(WebService+"-json",((JSONArray) object).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (MethodNotDefinedException e) {
                    e.printStackTrace();
                }
            }
            else if (object instanceof String) {
                try {
                    onCallWebServiceListener.OnGettingResult((String) object, flag);
                    lruCache.putResponse(WebService+"-xml",(String) object);
                }  catch (MethodNotDefinedException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected Object doInBackground(HashMap<String, String>... params) {
            if (responseType == JSON) {
                if (MethodType == GET) {
                   return getJSON(WebService, 15000);
                } else try {
                    return getJSONPOST(WebService, params[0]);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (responseType == XML) {
                if (MethodType == GET) {
                    String xmlData = getXML(WebService, 15000);
                    return xmlData;
                } else try {
                    String xmlData = getXMLPOST(WebService, params[0]);
                    return xmlData;
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }


    private Object getJSON(String url, int timeout) {
        HttpURLConnection c = null;
        try {
            URL u = new URL(url);
            c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setRequestProperty("Content-length", "0");
            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(timeout);
            c.setReadTimeout(timeout);
            c.connect();
            int status = c.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    System.out.println(sb.toString());
                    Object json = new JSONTokener(sb.toString()).nextValue();
                    return json;
            }

        } catch (MalformedURLException ex) {
        } catch (IOException ex) {
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.disconnect();
                } catch (Exception ex) {
                }
            }
        }
        return null;
    }


    private Object getJSONPOST(String url1, HashMap<String, String> apiParams) throws IOException, JSONException {
        URL url = new URL(url1);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);

        String getParams = getQuery(apiParams);

        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));
        writer.write(getParams);
        writer.flush();
        writer.close();
        os.close();
        conn.connect();

        int status = conn.getResponseCode();

        switch (status) {
            case 200:
            case 201:
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                System.out.println(sb.toString());
                Object json = new JSONTokener(sb.toString()).nextValue();
                return json;
        }


        if (conn != null) {
            try {
                conn.disconnect();
            } catch (Exception ex) {
            }
        }
        return null;
    }


    private String getXML(String url, int timeout) {
        HttpURLConnection c = null;
        try {
            URL u = new URL(url);
            c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setRequestProperty("Content-length", "0");
            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(timeout);
            c.setReadTimeout(timeout);
            c.connect();
            int status = c.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();

                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }
                    br.close();
                    return sb.toString();
            }

        } catch (MalformedURLException ex) {
        } catch (IOException ex) {
        } finally {
            if (c != null) {
                try {
                    c.disconnect();
                } catch (Exception ex) {
                }
            }
        }
        return null;
    }

    private String getXMLPOST(String url1, HashMap<String, String> apiParams) throws IOException, JSONException {
        URL url = new URL(url1);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);

        String getParams = getQuery(apiParams);

        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));
        writer.write(getParams);
        writer.flush();
        writer.close();
        os.close();
        conn.connect();

        int status = conn.getResponseCode();

        switch (status) {
            case 200:
            case 201:
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                br.close();

                System.out.println("aiyaz===>>  "+sb.toString());

                return sb.toString();
        }


        if (conn != null) {
            try {
                conn.disconnect();
            } catch (Exception ex) {
            }
        }
        return null;
    }


    private String getQuery(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;


        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return result.toString();
    }
}
