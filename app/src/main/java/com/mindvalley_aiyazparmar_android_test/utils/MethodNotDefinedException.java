package com.mindvalley_aiyazparmar_android_test.utils;

/**
 * Created by Aiyaz Parmar on 07/01/16.
 */
public class MethodNotDefinedException extends Exception {

    public MethodNotDefinedException(){

    }

    public MethodNotDefinedException(String Message){
        super(Message);
    }

}
