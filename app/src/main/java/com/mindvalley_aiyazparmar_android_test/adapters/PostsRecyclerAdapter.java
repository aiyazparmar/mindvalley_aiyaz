package com.mindvalley_aiyazparmar_android_test.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mindvalley_aiyazparmar_android_test.R;
import com.mindvalley_aiyazparmar_android_test.load_image_utils.LoadImage;
import com.mindvalley_aiyazparmar_android_test.pojo.MainPOJO.Post;

import java.util.List;

/**
 * Created by Aiyaz Parmar on 11/19/2016.
 */

public class PostsRecyclerAdapter extends RecyclerView.Adapter<PostsRecyclerAdapter.ViewHolder>{

    List<Post> postList;
    LoadImage loadImage=new LoadImage();
    Context context;


    //this adapter will work on both response xml and json

    public PostsRecyclerAdapter(List<Post> postList){
        this.postList=postList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context=parent.getContext();
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_post_recycler,parent,false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Post post=postList.get(position);
        if(holder!=null){

            //setting name
            holder.userName.setText(post.getUser().getName());

            //setting if user liked post or not
            if(post.getLikedByUser())holder.userLike.setImageResource(R.drawable.ic_heart_liked);
            else holder.userLike.setImageResource(R.drawable.ic_heart);

            //getting category name
            if(post.getCategories().size()>0)holder.imageCategory.setText(post.getCategories().get(0).getTitle());

            //setting image background color and
            holder.postImage.setBackgroundColor(Color.parseColor(post.getColor()));
            holder.postImage.setImageResource(0);



//            holder.postImage.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,post.getHeight()));

            //setting likes count
            holder.likesCount.setText(String.valueOf(post.getLikes()));



            //loading post image. if you want to cancel loading image just press on progress bar on top left corner


            LoadImage.LoadImageAsync loadImageAsync = null;
            if(post.getImageDownloading()==0){
                loadImageAsync = loadImage.displayImage(context,post.getUrls().getSmall(),holder.postImage,holder.imageProgressBar);
                holder.downloadImage.setVisibility(View.GONE);
            }else if(post.getImageDownloading()==1){
                holder.imageProgressBar.setVisibility(View.GONE);
                holder.downloadImage.setVisibility(View.VISIBLE);
                holder.postImage.setImageResource(0);
            }


            final LoadImage.LoadImageAsync finalLoadImageAsync = loadImageAsync;
            holder.imageProgressBar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    post.setImageDownloading(1);
                    finalLoadImageAsync.cancel(true);
                    holder.imageProgressBar.setVisibility(View.GONE);
                    holder.downloadImage.setVisibility(View.VISIBLE);
                }
            });

            holder.downloadImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    post.setImageDownloading(0);
                    loadImage.displayImage(context,post.getUrls().getSmall(),holder.postImage,holder.imageProgressBar);
                    holder.imageProgressBar.setVisibility(View.VISIBLE);
                    holder.downloadImage.setVisibility(View.GONE);
                }
            });


            //loading user image
            loadImage.displayImage(context,post.getUser().getProfileImage().getLarge(),holder.userImage,holder.userImageProgressBar,true);


            //setting when user like pic.
            holder.userLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(post.getLikedByUser()){
                        holder.userLike.setImageResource(R.drawable.ic_heart);
                        post.setLikes(post.getLikes()-1);
                        holder.likesCount.setText(String.valueOf(post.getLikes()));
                        post.setLikedByUser(false);
                    }else{
                        holder.userLike.setImageResource(R.drawable.ic_heart_liked);
                        post.setLikes(post.getLikes()+1);
                        holder.likesCount.setText(String.valueOf(post.getLikes()));
                        post.setLikedByUser(true);
                    }
                }
            });
        }else{

        }
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView postImage,userLike;
        private ImageView userImage,downloadImage;
        private TextView userName,imageCategory,likesCount;
        private ProgressBar userImageProgressBar,imageProgressBar;


        public ViewHolder(View itemView) {
            super(itemView);
            postImage=(ImageView)itemView.findViewById(R.id.post_image);
            userImage=(ImageView) itemView.findViewById(R.id.user_image);
            userLike=(ImageView)itemView.findViewById(R.id.user_like);
            downloadImage=(ImageView)itemView.findViewById(R.id.download_image);
            userName=(TextView)itemView.findViewById(R.id.username);
            imageCategory=(TextView)itemView.findViewById(R.id.image_category);
            likesCount=(TextView)itemView.findViewById(R.id.likes_count);
            userImageProgressBar=(ProgressBar) itemView.findViewById(R.id.progressBar_userImage);
            imageProgressBar=(ProgressBar) itemView.findViewById(R.id.progressBar);
        }
    }
}