package com.mindvalley_aiyazparmar_android_test.url_caching;

import android.graphics.Bitmap;
import android.util.Log;

/**
 * Created by Aiyaz Parmar on 20/11/16.
 */
public  class LruCache extends android.util.LruCache<String,String> {

    public static int getDefaultLruCacheSize() {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;

        return cacheSize;
    }

    public LruCache() {
        this(getDefaultLruCacheSize());
    }

    public LruCache(int sizeInKiloBytes) {
        super(sizeInKiloBytes);
    }

    public String getResponse(String url) {
        return this.get(url);
    }

    public void putResponse(String url, String response) {
        Log.e("aiyaz","putting bitmap");
        this.put(url, response);
    }
}
