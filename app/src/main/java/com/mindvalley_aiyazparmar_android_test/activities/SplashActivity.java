package com.mindvalley_aiyazparmar_android_test.activities;



import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.mindvalley_aiyazparmar_android_test.R;
import com.mindvalley_aiyazparmar_android_test.utils.BaseActivity;

/**
 * Created by Aiyaz Parmar on 11/19/2016.
 */

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(SplashActivity.this,DashboardActivity.class);
                startActivity(intent);
            }
        },2000);
    }
}
