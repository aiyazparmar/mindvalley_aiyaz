package com.mindvalley_aiyazparmar_android_test.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.mindvalley_aiyazparmar_android_test.R;
import com.mindvalley_aiyazparmar_android_test.adapters.PostsRecyclerAdapter;
import com.mindvalley_aiyazparmar_android_test.pojo.MainPOJO.Category;
import com.mindvalley_aiyazparmar_android_test.pojo.MainPOJO.Post;
import com.mindvalley_aiyazparmar_android_test.pojo.MainPOJO.ProfileImage;
import com.mindvalley_aiyazparmar_android_test.pojo.MainPOJO.Urls;
import com.mindvalley_aiyazparmar_android_test.pojo.MainPOJO.User;
import com.mindvalley_aiyazparmar_android_test.utils.BaseActivity;
import com.mindvalley_aiyazparmar_android_test.utils.CallWebService;
import com.mindvalley_aiyazparmar_android_test.utils.OnCallWebServiceListener;
import com.mindvalley_aiyazparmar_android_test.utils.MethodNotDefinedException;
import com.mindvalley_aiyazparmar_android_test.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Aiyaz Parmar on 11/19/16.
 */
public class DashboardActivity extends BaseActivity {

    private static int MainPageWebService = 0;
    private RecyclerView postsRecyclerView;
    private SwipeRefreshLayout refreshLoadedData;

    private static String TAG = "Dashboard";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard); //setting view for dashboard activity


        initializeWidgets();  //to initialize all UI controls.


        //here i have also created one more webservice of xml if you want response in xml or your response is in xml then just
        //uncomment that callWebService like of XML.

        try {
            // Calling xml webservice.
//            new CallWebService(CallWebService.XML, CallWebService.GET, "http://pastebin.com/raw/LNFBgNry", null, onCallWebServiceListener, MainPageWebService);

            // Calling JSON webservice.
            new CallWebService(CallWebService.JSON, CallWebService.GET, "http://pastebin.com/raw/wgkJgazE", null,onCallWebServiceListener ,MainPageWebService);
        } catch (MethodNotDefinedException e) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return false;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initializeWidgets() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        postsRecyclerView = (RecyclerView) findViewById(R.id.post_feed_recycler);
        refreshLoadedData = (SwipeRefreshLayout) findViewById(R.id.swipe_to_refresh);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Hello There..!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        // Called when pull to refresh action performed.
        refreshLoadedData.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    refreshLoadedData.setRefreshing(true);
                    // Calling xml webservice.

//                    new CallWebService(CallWebService.XML, CallWebService.GET, "http://pastebin.com/raw/LNFBgNry", null, onCallWebServiceListener, MainPageWebService);

                    // Calling JSON webservice.
                    new CallWebService(CallWebService.JSON, CallWebService.GET, "http://pastebin.com/raw/wgkJgazE", null,onCallWebServiceListener ,MainPageWebService);
                } catch (MethodNotDefinedException e) {

                }
            }
        });

    }


    OnCallWebServiceListener onCallWebServiceListener = new OnCallWebServiceListener() {

        MaterialDialog materialDialog;

        @Override
        public void OnStartingService() {
            if (refreshLoadedData.isRefreshing()) refreshLoadedData.setRefreshing(false);
            materialDialog = Utils.getProgressDialog(DashboardActivity.this);
            materialDialog.show();
        }

        @Override
        public void OnGettingResult(JSONObject jsonObject, int flag) throws JSONException, MethodNotDefinedException {
//            Toast.makeText(getApplicationContext(), "Hello", Toast.LENGTH_LONG).show();
            Log.e(TAG, "response in json object");

        }

        @Override
        public void OnGettingResult(JSONArray jsonArray, int flag) throws JSONException, MethodNotDefinedException {

            if (refreshLoadedData.isRefreshing()) refreshLoadedData.setRefreshing(false);
            if (materialDialog.isShowing()) materialDialog.dismiss();

            if (flag == MainPageWebService) {
                Post[] posts = new Gson().fromJson(jsonArray.toString(), Post[].class);
                List<Post> tempPosts = Arrays.asList(posts);
                List<Post> postList = new ArrayList<>();
                postList.addAll(tempPosts);
                for (int i = 0; i < postList.size(); i++) postList.get(i).setImageDownloading(0);
                PostsRecyclerAdapter postsRecyclerAdapter = new PostsRecyclerAdapter(postList);
                postsRecyclerView.setLayoutManager(new LinearLayoutManager(DashboardActivity.this));
                postsRecyclerView.setAdapter(postsRecyclerAdapter);
            }
        }

        @Override
        public void OnGettingResult(String xmlData, int flag) throws MethodNotDefinedException {
            if (refreshLoadedData.isRefreshing()) refreshLoadedData.setRefreshing(false);
            if (materialDialog.isShowing()) materialDialog.dismiss();
            if (flag == MainPageWebService) {
                try {
                    xmlParsing(xmlData);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error in parsing XML", Toast.LENGTH_LONG).show();
                }
            }

        }
    };


    private void xmlParsing(String xmlString) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        try {
            db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xmlString));
            try {
                Document doc = db.parse(is);
                doc.getDocumentElement().normalize();

                List<Post> postList = new ArrayList<>();

                NodeList nList = doc.getElementsByTagName("single_item");
                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);
                    Post post = new Post();
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;

                        System.out.println("\nCurrent Element :"
                                + eElement.getElementsByTagName("id").item(0)
                                .getTextContent());
                        post.setId(eElement.getElementsByTagName("id").item(0)
                                .getTextContent());
                        post.setCreatedAt(eElement.getElementsByTagName("created_at").item(0)
                                .getTextContent());
                        post.setWidth(Integer.parseInt(eElement.getElementsByTagName("width").item(0)
                                .getTextContent()));
                        post.setHeight(Integer.parseInt(eElement.getElementsByTagName("height").item(0)
                                .getTextContent()));
                        post.setColor(eElement.getElementsByTagName("color").item(0)
                                .getTextContent());
                        post.setLikes(Integer.parseInt(eElement.getElementsByTagName("likes").item(0)
                                .getTextContent()));
                        post.setLikedByUser(Boolean.getBoolean(eElement.getElementsByTagName("liked_by_user").item(0)
                                .getTextContent()));


                        NodeList nUser = eElement.getElementsByTagName("user");
                        for (int k = 0; k < nUser.getLength(); k++) {
                            Node nNodeUser = nUser.item(k);

                            if (nNodeUser.getNodeType() == Node.ELEMENT_NODE) {
                                Element nUserElement = (Element) nNodeUser;

                                User user = new User();
                                Log.e("aiyaz", nUserElement.getElementsByTagName("username").item(0)
                                        .getTextContent());
                                user.setId(nUserElement.getElementsByTagName("id").item(0)
                                        .getTextContent());
                                user.setUsername(nUserElement.getElementsByTagName("username").item(0)
                                        .getTextContent());
                                user.setName(nUserElement.getElementsByTagName("name").item(0)
                                        .getTextContent());


                                System.out.println("\nCurrent User :"
                                        + nUserElement.getElementsByTagName("username").item(0)
                                        .getTextContent());


                                NodeList nProfileImage = nUserElement.getElementsByTagName("profile_image");
                                for (int l = 0; l < nProfileImage.getLength(); l++) {
                                    Node nNodeProfileImage = nUser.item(l);
                                    if (nNodeProfileImage.getNodeType() == Node.ELEMENT_NODE) {
                                        Element nProfileImageElement = (Element) nNodeProfileImage;
                                        ProfileImage profileImage = new ProfileImage();
                                        profileImage.setSmall(nProfileImageElement.getElementsByTagName("small").item(0)
                                                .getTextContent());
                                        profileImage.setMedium(nProfileImageElement.getElementsByTagName("medium").item(0)
                                                .getTextContent());
                                        profileImage.setLarge(nProfileImageElement.getElementsByTagName("large").item(0)
                                                .getTextContent());
                                        user.setProfileImage(profileImage);
                                    }

                                }
                                post.setUser(user);


                            }
                        }


                        NodeList nUrls = eElement.getElementsByTagName("urls");
                        for (int k = 0; k < nUrls.getLength(); k++) {
                            Node nNodeUrls = nUrls.item(k);
                            if (nNodeUrls.getNodeType() == Node.ELEMENT_NODE) {
                                Element nUrlElement = (Element) nNodeUrls;
                                Urls urls = new Urls();
                                urls.setRaw(nUrlElement.getElementsByTagName("raw").item(0)
                                        .getTextContent());
                                urls.setSmall(nUrlElement.getElementsByTagName("small").item(0)
                                        .getTextContent());
                                urls.setFull(nUrlElement.getElementsByTagName("full").item(0)
                                        .getTextContent());
                                urls.setRegular(nUrlElement.getElementsByTagName("regular").item(0)
                                        .getTextContent());
                                urls.setThumb(nUrlElement.getElementsByTagName("thumb").item(0)
                                        .getTextContent());
                                post.setUrls(urls);

                            }

                        }


                        NodeList nCategories = doc.getElementsByTagName("categories");
                        List<Category> categories = new ArrayList<>();
                        for (int j = 0; j < nCategories.getLength(); j++) {
                            Node nNodeCategory = nCategories.item(j);
                            if (nNodeCategory.getNodeType() == Node.ELEMENT_NODE) {
                                Element eCategories = (Element) nNodeCategory;
                                Category category = new Category();
                                category.setId(Integer.parseInt(eCategories.getElementsByTagName("id").item(0)
                                        .getTextContent()));
                                category.setTitle(eCategories.getElementsByTagName("title").item(0)
                                        .getTextContent());
                                category.setPhotoCount(Integer.parseInt(eCategories.getElementsByTagName("photo_count").item(0)
                                        .getTextContent()));
                                categories.add(category);
                            }

                        }
                        post.setCategories(categories);
                        post.setImageDownloading(0);
                        postList.add(post);
                    }

                }

                PostsRecyclerAdapter postsRecyclerAdapter = new PostsRecyclerAdapter(postList);
                postsRecyclerView.setLayoutManager(new LinearLayoutManager(DashboardActivity.this));
                postsRecyclerView.setAdapter(postsRecyclerAdapter);
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                // handle IOException
                e.printStackTrace();
            }
        } catch (ParserConfigurationException e1) {
            // handle ParserConfigurationException
            e1.printStackTrace();
        }
    }
}
