package com.mindvalley_aiyazparmar_android_test.load_image_utils;

import android.graphics.Bitmap;
import android.util.Log;

/**
 * Created by Aiyaz Parmar on 20/11/16.
 */
public  class LruCache extends android.util.LruCache<String,Bitmap> {

    public static int getDefaultLruCacheSize() {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;

        return cacheSize;
    }

    public LruCache() {
        this(getDefaultLruCacheSize());
    }

    public LruCache(int sizeInKiloBytes) {
        super(sizeInKiloBytes);
    }

    @Override
    protected int sizeOf(String key, Bitmap value) {
        return value.getRowBytes() * value.getHeight() / 1024;
    }


    public Bitmap getBitmap(String url) {
        return this.get(url);
    }

    public void putBitmap(String url, Bitmap bitmap) {
        Log.e("aiyaz","putting bitmap");
        this.put(url, bitmap);
    }
}
