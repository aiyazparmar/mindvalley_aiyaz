package com.mindvalley_aiyazparmar_android_test.load_image_utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Aiyaz Parmar on 20/11/16.
 */
public class LoadImage {

    Context context;

    private static String TAG="LoadImage";

    static LruCache lruCache=new LruCache();
    boolean isRoundedCorner;


    public LoadImageAsync displayImage(Context context,String url, ImageView imageView){
        this.context=context;
        if(lruCache.getBitmap(url)!=null){
            imageView.setImageBitmap(lruCache.getBitmap(url));
            return null;
        }else{
            Log.e(TAG,"image from display "+url);
            imageView.setImageResource(0);
            LoadImageAsync loadImageAsync =  new LoadImageAsync(imageView);
            loadImageAsync.execute(url);
            return loadImageAsync;
        }
    }

    public LoadImageAsync displayImage(Context context, String url, ImageView imageView, ProgressBar progressBar){
        isRoundedCorner=false;
        this.context=context;
        if(lruCache.getBitmap(url)!=null){
            progressBar.setVisibility(View.GONE);
            imageView.setImageBitmap(lruCache.getBitmap(url));

            return null;
        }else{
            Log.e(TAG,"image from display "+url);
            progressBar.setVisibility(View.VISIBLE);
            imageView.setImageResource(0);
            LoadImageAsync loadImageAsync = new LoadImageAsync(imageView,progressBar);
            loadImageAsync.execute(url);
            return loadImageAsync;
        }
    }

    public LoadImageAsync displayImage(Context context, String url, ImageView imageView, ProgressBar progressBar,boolean isRoundedCorner){
        this.isRoundedCorner=isRoundedCorner;
        this.context=context;
        if(lruCache.getBitmap(url)!=null){
            progressBar.setVisibility(View.GONE);
            if(!isRoundedCorner) imageView.setImageBitmap(lruCache.getBitmap(url));
            else imageView.setImageBitmap(getRoundedCornerBitmap(lruCache.getBitmap(url)));
            return null;
        }else{
            Log.e(TAG,"image from display "+url);
            imageView.setImageResource(0);
            LoadImageAsync loadImageAsync = new LoadImageAsync(imageView,progressBar);
            loadImageAsync.execute(url);
            return loadImageAsync;
        }
    }


    public  class LoadImageAsync extends AsyncTask<String,Void,Bitmap>{

        ImageView imageView;
        String imageUrl;
        ProgressBar progressBar=null;
        public LoadImageAsync(ImageView imageView){
            this.imageView=imageView;
        }
        public LoadImageAsync(ImageView imageView,ProgressBar progressBar){
            this.imageView=imageView;
            this.progressBar=progressBar;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(final Bitmap bitmap) {
            super.onPostExecute(bitmap);

            if(progressBar!=null)progressBar.setVisibility(View.GONE);
            if(bitmap!=null) {

                TransitionDrawable td;
                if (!isRoundedCorner)
                    td = new TransitionDrawable(new Drawable[]{
                                    new ColorDrawable(Color.TRANSPARENT),
                                    new BitmapDrawable(context.getResources(), bitmap)
                            });
                else
                   td = new TransitionDrawable(new Drawable[]{
                                    new ColorDrawable(Color.TRANSPARENT),
                                    new BitmapDrawable(context.getResources(), getRoundedCornerBitmap(bitmap))
                            });

                imageView.setImageDrawable(td);
                td.startTransition(1000);
            }else{
                imageView.setImageResource(0);
            }

        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            imageUrl=strings[0];
            return download_Image(strings[0],imageView);
        }

        @Override
        protected void onCancelled(Bitmap bitmap) {
            super.onCancelled(bitmap);
            onPostExecute(null);
        }
    }

    private  Bitmap download_Image(String url,ImageView imageView) {

        Bitmap bmp =null;
        try{
            URL ulrn = new URL(url);
            HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
            InputStream is = con.getInputStream();
            bmp = BitmapFactory.decodeStream(is);
            if (null != bmp) {
                lruCache.putBitmap(url,bmp);
                return bmp;
            }else{
            }
        }catch(Exception e){

        }
        return bmp;
    }



    public Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = 5;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }
}